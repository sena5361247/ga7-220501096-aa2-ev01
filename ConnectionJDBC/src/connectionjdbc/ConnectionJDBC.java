/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package connectionjdbc;

/**
 *
 * @author Bairon
 */
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
public class ConnectionJDBC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        String usuario = "root";
        String password = "";
        String url = "jdbc:mysql://localhost:3306/photovoltaics";
        Connection conexion;
        Statement statement;
        ResultSet rs;
                
        try {
            
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConnectionJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            conexion = DriverManager.getConnection(url, usuario, password);
            statement = conexion.createStatement();
            System.out.println("Conexion exitosa");
            String [] datos = new String [6] ;
            int opcion; 
                opcion = Integer.parseInt(JOptionPane.showInputDialog("Por favor digite que opcion quiere realizar_1. Insertar. 2. Consultar. 3. Actualizar. 4. Eliminar"));
                    
           if (opcion == 1){
            for (int i = 0; i < 6; i++) {
                if (i == 0){
                    datos[i]=JOptionPane.showInputDialog("Por favor digite la Descripcion del item");
                }
                if (i == 1){
                    datos[i]=JOptionPane.showInputDialog("Por favor digite el No_Parte del item");
                }
                if (i == 2){
                    datos[i]=JOptionPane.showInputDialog("Por favor digite la Cantidad del item");
                }
                if (i == 3){
                    datos[i]=JOptionPane.showInputDialog("Por favor digite la Unidad del item");
                }
                if (i == 4){
                    datos[i]=JOptionPane.showInputDialog("Por favor digite la Voltaje del item");
                }
                if (i == 5){
                    datos[i]=JOptionPane.showInputDialog("Por favor digite la Corriente del item");
                }
            }
            String Adicionar = "INSERT INTO BATERIA (Descripcion,No_Parte,Cantidad,Unidad,Voltaje,Corriente) VALUES ('"+datos[0]+"','"+datos[1]+"',"+datos[2]+",'"+datos[3]+"',"+datos[4]+","+datos[5]+")";
            statement.executeUpdate(Adicionar);
           }
           if (opcion == 2){
            rs = statement.executeQuery("SELECT * FROM BATERIA");
            rs.next();
            do{
               System.out.println(rs.getInt("id_BATERIA")+":"+rs.getString("Descripcion")); 
            }while(rs.next());
           } 
           if (opcion == 3){
            for (int i = 0; i < 6; i++) {
                if (i == 0){
                    datos[i]=JOptionPane.showInputDialog("Por favor digite la Descripcion del item");
                }
                if (i == 1){
                    datos[i]=JOptionPane.showInputDialog("Por favor digite el No_Parte del item");
                }
                if (i == 2){
                    datos[i]=JOptionPane.showInputDialog("Por favor digite la Cantidad del item");
                }
                if (i == 3){
                    datos[i]=JOptionPane.showInputDialog("Por favor digite la Unidad del item");
                }
                if (i == 4){
                    datos[i]=JOptionPane.showInputDialog("Por favor digite la Voltaje del item");
                }
                if (i == 5){
                    datos[i]=JOptionPane.showInputDialog("Por favor digite la Corriente del item");
                }
            } 
            
            statement.executeUpdate("UPDATE BATERIA SET Descripcion = '"+datos[0]+"', No_Parte = '"+datos[1]+"',Cantidad = "+datos[2]+",Unidad = '"+datos[3]+"',Voltaje = "+datos[4]+",Corriente = "+datos[5]+" WHERE id_BATERIA = 1");
           }
           if (opcion == 4){
           int Borrar;
           Borrar=Integer.parseInt(JOptionPane.showInputDialog("Por favor digite el ID del item a borrar"));
           statement.executeUpdate("DELETE FROM BATERIA WHERE id_BATERIA = "+Borrar+"");
           }
           }
           
            
         catch (SQLException ex) {
            Logger.getLogger(ConnectionJDBC.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
}
